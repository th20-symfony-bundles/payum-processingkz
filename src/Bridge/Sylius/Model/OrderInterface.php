<?php

namespace Payum\Processingkz\Bridge\Sylius\Model;


use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;


interface OrderInterface
{

    /**
     * Name of a currency for convertion - KZT.
     */
    const CURRENCY_NAME_KZT = 398;


    /**
     * Returns an order payment object.
     *
     * @return PaymentInterface|null
     *   An instance of PaymentInterface if there is one.
     */
    public function getPayment();

    /**
     * Returns an order total.
     *
     * @return integer
     */
    public function getTotal();

    /**
     * Returns an order total converted and normalized to the given currency.
     *
     * @param int $currencyName
     *   One of the CURRENCY_NAME_* interface constants. To what currency
     *   should the order total be converted.
     *
     * @return integer
     */
    public function getTotalInCurrency($currencyName);

}
