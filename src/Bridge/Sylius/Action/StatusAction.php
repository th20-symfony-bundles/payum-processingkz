<?php

namespace Payum\Processingkz\Bridge\Sylius\Action;


use Exception;

use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\SyncRequest;
use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;

use Payum\Processingkz\Action\BaseApiAwareAction;
use Payum\Processingkz\Api;
use Payum\Processingkz\Bridge\Sylius\Request\StatusRequest;


class StatusAction extends BaseApiAwareAction
{

    /**
     * {@inheritdoc}
     */
    public function execute($request)
    {
        if (!$this->supports($request)) {
            throw RequestNotSupportedException::createActionNotSupported($this, $request);
        }

        $payment = $request->getModel();
        $paymentDetails = $payment->getDetails();

        if (empty($paymentDetails['customerReference'])) {
            $request->markNew();
            return;
        }

        if (!empty($paymentDetails['transactionStatus'])) {
            // First, check terminal statuses. Status cannot be changed anymore,
            // once in a terminal state, so there is no need to sync again.
            switch ($paymentDetails['transactionStatus']) {
                case 'PAID':
                    $request->markSuccess();
                    return;

                case 'REFUNDED':
                    $request->markRefunded();
                    return;

                case 'DECLINED':
                    $request->markFailed();
                    return;

                case 'REVERSED':
                    $request->markCanceled();
                    return;

                case 'INVALID_MID':
                case 'MID_DISABLED':
                    $request->markExpired();
                    return;
            }
        }

        // If transaction status is not yet terminal, syncronize it.
        $this->payment->execute(new SyncRequest($payment));

        $paymentDetails = $payment->getDetails();
        switch ($paymentDetails['transactionStatus']) {
            case 'PAID':
                $request->markSuccess();
                break;

            case 'AUTHORISED':
                $request->markSuspended();
                break;

            case 'PENDING_CUSTOMER_INPUT':
            case 'PENDING_AUTH_RESULT':
                $request->markPending();
                break;

            case 'REFUNDED':
                $request->markRefunded();
                break;

            case 'DECLINED':
                $request->markFailed();
                break;

            case 'REVERSED':
                $request->markCanceled();
                break;

            case 'NO_SUCH_TRANSACTION':
            case 'INVALID_MID':
            case 'MID_DISABLED':
                $request->markExpired();
                break;

            default:
                $request->markUnknown();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request)
    {
        return
            $request instanceof StatusRequest &&
            $request->getModel() instanceof PaymentInterface;
    }

}
