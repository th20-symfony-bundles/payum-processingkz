<?php

namespace Payum\Processingkz\Bridge\Sylius\Action;


use Exception;

use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Request\RedirectUrlInteractiveRequest;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\RuntimeException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\CaptureRequest;
use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

use Payum\Processingkz\Action\BaseApiAwareAction;
use Payum\Processingkz\Api;
use Payum\Processingkz\Bridge\Sylius\Model\OrderInterface;
use Payum\Processingkz\Bridge\Sylius\Request\StatusRequest;
use Payum\Processingkz\Bridge\Symfony\Model\Event\Dispatcher;
use Payum\Processingkz\Bridge\Symfony\Model\Event\TransactionStartEvent;


class CaptureAction extends BaseApiAwareAction
{

    const SESSION_KEY = 'payum_processingkz_sylius_captureaction_userguard';


    protected $eventDispatcher;

    protected $session;


    public function __construct(SessionInterface $session, Dispatcher $eventDispatcher)
    {
        $this->session = $session;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($request)
    {
        if (!$this->supports($request)) {
            throw RequestNotSupportedException::createActionNotSupported($this, $request);
        }

        $order = $request->getModel();
        $payment = $order->getPayment();

        $payment->setAmount($order->getTotalInCurrency(OrderInterface::CURRENCY_NAME_KZT));
        $payment->setCurrency('KZT');
        $payment->setDetails($payment->getDetails() + array(
            'currency_code' => $this->api->currencyCodeFromName('KZT'),
            'description'   => strval($order),
            'order_id'      => $order->getId(),
            'return_url'    => $request->getToken()->getTargetUrl(),
            'total'         => $payment->getAmount(),
        ));

        // Check transaction status and report back to the payment model.
        if (!array_key_exists('customerReference', $payment->getDetails())) {
            $paymentDetails = ArrayObject::ensureArrayObject($payment->getDetails());

            $event = new TransactionStartEvent();
            $event->setTransactionDetails($paymentDetails);
            $this->eventDispatcher->dispatchTransactionStart($event);

            try {
                $this->startTransaction($paymentDetails);
            } catch (Exception $e) {
                $payment->setDetails($paymentDetails->toUnsafeArray());
                throw $e;
            }

            // Unreachable. startTransaction() always throws.
            throw new RuntimeException('Failed assertion. CaptureAction::startTransaction() method did not throw an exception.');
        }

        $status = new StatusRequest($payment);
        $this->payment->execute($status);

        // This request comes from a client, or is it an automatic notification
        // from a payment gateway...
        if (!$this->session->has(self::SESSION_KEY)) {
            // The payment gateway automaticaly requests the 'return' URL when
            // a user completes payment. If we return from the action now, the
            // security token will be invalidated, and the real user will get a
            // 404 error when they return to the site. Throw a redirection
            // exception to send away the bot, and wait for the real user to
            // return.
            throw new RedirectUrlInteractiveRequest('http://localhost/');
        }

        // Capture is complete. Return, unvalidate token, redirect to 'after' URL.
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request)
    {
        return
            $request instanceof CaptureRequest &&
            $request->getModel() instanceof OrderInterface &&
            $request->getModel()->getPayment() instanceof PaymentInterface;
    }

    protected function startTransaction($paymentDetails)
    {
        $start = $this->api->startTransaction($paymentDetails);
        $paymentDetails->replace($start->toArray());

        if ($start->getSuccess()) {
            // Add our state flag the session to distinguish between a real
            // user and an automatic request.
            $this->session->set(self::SESSION_KEY, mt_rand());

            throw new RedirectUrlInteractiveRequest($start->getRedirectUrl());
        }

        $message = sprintf('Could not start payment transaction: %s', $start->getErrorDescription());
        throw new RuntimeException($message);
    }

}
