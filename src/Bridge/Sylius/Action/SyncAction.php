<?php

namespace Payum\Processingkz\Bridge\Sylius\Action;


use Exception;

use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\SyncRequest;
use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;

use Payum\Processingkz\Action\BaseApiAwareAction;
use Payum\Processingkz\Api;
use Payum\Processingkz\Bridge\Symfony\Model\Event\Dispatcher;
use Payum\Processingkz\Bridge\Symfony\Model\Event\TransactionStatusSyncEvent;


class SyncAction extends BaseApiAwareAction
{

    protected $eventDispatcher;


    public function __construct(Dispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($request)
    {
        if (!$this->supports($request)) {
            throw RequestNotSupportedException::createActionNotSupported($this, $request);
        }

        $payment = $request->getModel();
        $paymentDetails = $payment->getDetails();
        $paymentDetails = ArrayObject::ensureArrayObject($paymentDetails);

        if (empty($paymentDetails['customerReference'])) {
            return;
        }

        $event = new TransactionStatusSyncEvent();
        if (!empty($paymentDetails['transactionStatus'])) {
            $event->setOldStatus($paymentDetails['transactionStatus']);
        }

        $status = $this->api->checkStatus($paymentDetails);

        $paymentDetails->replace($status->toArray());
        $payment->setDetails($paymentDetails->toUnsafeArray());

        $event->setNewStatus($paymentDetails['transactionStatus']);
        $this->eventDispatcher->dispatchTransactionStatusSync($event);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request)
    {
        return
            $request instanceof SyncRequest &&
            $request->getModel() instanceof PaymentInterface;
    }

}
