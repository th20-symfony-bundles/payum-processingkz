<?php

namespace Payum\Processingkz\Bridge\Sylius\Action;


use Exception;

use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Request\RedirectUrlInteractiveRequest;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\RuntimeException;
use Payum\Core\Exception\UnsupportedApiException;
use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;

use Payum\Processingkz\Action\BaseApiAwareAction;
use Payum\Processingkz\Api;
use Payum\Processingkz\Bridge\Sylius\Model\OrderInterface;
use Payum\Processingkz\Bridge\Sylius\Request\StatusRequest;
use Payum\Processingkz\Request\CompleteRequest;


class CompleteAction extends BaseApiAwareAction
{

    /**
     * {@inheritdoc}
     */
    public function execute($request)
    {
        if (!$this->supports($request)) {
            throw RequestNotSupportedException::createActionNotSupported($this, $request);
        }

        $payment = $request->getModel();
        $paymentDetails = $payment->getDetails();

        $this->api->completeTransaction($paymentDetails, Api::COMPLETE_SUCCESS);

        $status = new StatusRequest($payment);
        $this->payment->execute($status);
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request)
    {
        return
            $request instanceof CompleteRequest &&
            $request->getModel() instanceof PaymentInterface;
    }

}
