<?php

namespace Payum\Processingkz\Bridge\Sylius\Bundle\ProcessingkzSyliusBundle\DependencyInjection\Factory\Payment;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

use Payum\Processingkz\Bridge\Symfony\Bundle\ProcessingkzBundle\DependencyInjection\Factory\Payment\BasePaymentFactory;


class ProcessingkzSyliusPaymentFactory extends BasePaymentFactory
{

    /**
     * {@inheritdoc}
     */
    protected function addActions(Definition $paymentDefinition, ContainerBuilder $container, $contextName, array $config)
    {
        $actionId = 'sylius.payum.processingkz.action.capture';
        $paymentDefinition->addMethodCall('addAction', array(new Reference($actionId)));

        $actionId = 'sylius.payum.processingkz.action.status';
        $paymentDefinition->addMethodCall('addAction', array(new Reference($actionId)));

        $actionId = 'sylius.payum.processingkz.action.sync';
        $paymentDefinition->addMethodCall('addAction', array(new Reference($actionId)));

        $actionId = 'sylius.payum.processingkz.action.complete';
        $paymentDefinition->addMethodCall('addAction', array(new Reference($actionId)));

        $actionId = 'sylius.payum.processingkz.action.cancel';
        $paymentDefinition->addMethodCall('addAction', array(new Reference($actionId)));
    }

}
