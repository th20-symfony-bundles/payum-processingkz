<?php

namespace Payum\Processingkz\Bridge\Sylius\Bundle\ProcessingkzSyliusBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

use Payum\Processingkz\Bridge\Symfony\Bundle\ProcessingkzBundle\DependencyInjection\Configuration as BaseConfiguration;

/**
 * Validates and merges configuration for the bundle.
 */
class Configuration extends BaseConfiguration
{
}
