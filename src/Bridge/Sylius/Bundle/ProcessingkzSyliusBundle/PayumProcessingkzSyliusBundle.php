<?php

namespace Payum\Processingkz\Bridge\Sylius\Bundle\ProcessingkzSyliusBundle;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use Payum\Processingkz\Bridge\Sylius\Bundle\ProcessingkzSyliusBundle\DependencyInjection\Factory\Payment\ProcessingkzSyliusPaymentFactory;


class PayumProcessingkzSyliusBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // Prevent possible conflicts with another bundle from this package.
        if ($container->hasExtension('payum_processingkz')) {
            throw new LogicException('PayumProcessingkzSyliusBundle conflicts with PayumProcessingkzBundle. Please disable PayumProcessingkzBundle.');
        }

        // Autoconfigure extension if it dependencies exist.
        $configure =
            $container->hasExtension('payum') &&
            $container->hasExtension('sylius_payum');

        if ($configure) {
            $factory = new ProcessingkzSyliusPaymentFactory();

            $payum = $container->getExtension('payum');
            $payum->addPaymentFactory($factory);
        }
    }

}
