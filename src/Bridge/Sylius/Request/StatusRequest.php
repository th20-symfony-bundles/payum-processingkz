<?php

namespace Payum\Processingkz\Bridge\Sylius\Request;


use Payum\Core\Request\BaseStatusRequest;
use Sylius\Bundle\PaymentsBundle\Model\PaymentInterface;


class StatusRequest extends BaseStatusRequest
{

    public function __construct($model)
    {
        if (!($model instanceof PaymentInterface)) {
            throw new InvalidArgumentException('StatusRequest can work only with instances of PaymentInterface from Sylius package.');
        }

        parent::__construct($model);
    }

    public function getStatus()
    {
        return $this->model->getState();
    }

    public function markNew()
    {
        $this->model->setState(PaymentInterface::STATE_NEW);
    }

    public function isNew()
    {
        return $this->getStatus() == PaymentInterface::STATE_NEW;
    }

    public function markSuccess()
    {
        $this->model->setState(PaymentInterface::STATE_COMPLETED);
    }

    public function isSuccess()
    {
        return $this->getStatus() == PaymentInterface::STATE_COMPLETED;
    }

    public function markSuspended()
    {
        $this->model->setState(PaymentInterface::STATE_PROCESSING);
    }

    public function isSuspended()
    {
        return $this->getStatus() == PaymentInterface::STATE_PROCESSING;
    }

    public function markExpired()
    {
        $this->model->setState(PaymentInterface::STATE_VOID);
    }

    public function isExpired()
    {
        return $this->getStatus() == PaymentInterface::STATE_VOID;
    }

    public function markCanceled()
    {
        $this->model->setState(PaymentInterface::STATE_CANCELLED);
    }

    public function isCanceled()
    {
        return $this->getStatus() == PaymentInterface::STATE_CANCELLED;
    }

    public function markPending()
    {
        $this->model->setState(PaymentInterface::STATE_PENDING);
    }

    public function isPending()
    {
        return $this->getStatus() == PaymentInterface::STATE_PENDING;
    }

    public function markFailed()
    {
        $this->model->setState(PaymentInterface::STATE_FAILED);
    }

    public function isFailed()
    {
        return $this->getStatus() == PaymentInterface::STATE_FAILED;
    }

    public function markUnknown()
    {
        $this->model->setState(PaymentInterface::STATE_UNKNOWN);
    }

    public function isUnknown()
    {
        return $this->getStatus() == PaymentInterface::STATE_UNKNOWN;
    }

    /**
     * Marks request is 'refunded'.
     *
     * Non-standard state for Payum status requests.
     */
    public function markRefunded()
    {
        $this->model->setState(PaymentInterface::STATE_REFUNDED);
    }

    /**
     * Checks if request is marked as 'refunded'.
     *
     * Non-standard state for Payum status requests.
     */
    public function isRefunded()
    {
        return $this->getStatus() == PaymentInterface::STATE_REFUNDED;
    }

}
