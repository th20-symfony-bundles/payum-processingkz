<?php

namespace Payum\Processingkz\Bridge\Symfony\Bundle\ProcessingkzBundle;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

use Payum\Processingkz\Bridge\Symfony\Bundle\ProcessingkzBundle\DependencyInjection\Factory\Payment\ProcessingkzPaymentFactory;


class PayumProcessingkzBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // Autoconfigure 'payum' extension if it exists.
        if ($container->hasExtension('payum')) {
            $factory = new ProcessingkzPaymentFactory();

            $payum = $container->getExtension('payum');
            $payum->addPaymentFactory($factory);
        }
    }

}
