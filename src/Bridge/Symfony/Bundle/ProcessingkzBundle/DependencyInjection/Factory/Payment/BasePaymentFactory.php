<?php

namespace Payum\Processingkz\Bridge\Symfony\Bundle\ProcessingkzBundle\DependencyInjection\Factory\Payment;


use Payum\Bundle\PayumBundle\DependencyInjection\Factory\Payment\AbstractPaymentFactory;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;


class BasePaymentFactory extends AbstractPaymentFactory
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'processingkz';
    }

    /**
     * {@inheritdoc}
     */
    public function addConfiguration(ArrayNodeDefinition $builder)
    {
        parent::addConfiguration($builder);

        $builder
            ->children()
                ->arrayNode('api')->isRequired()
                    ->children()
                        ->arrayNode('options')->isRequired()
                            ->children()
                                ->scalarNode('merchant')
                                    ->isRequired()->cannotBeEmpty()
                                ->end()
                                ->scalarNode('wsdl')
                                    ->defaultValue(null)
                                ->end()
                                ->scalarNode('location')
                                    ->defaultValue(null)
                                ->end()
                            ->end()
                    ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function addApis(Definition $paymentDefinition, ContainerBuilder $container, $contextName, array $config)
    {
        $apiDefinition = new DefinitionDecorator('payum.processingkz.processingkz_soap.api');
        $apiDefinition->replaceArgument(0, $config['api']['options']);
        $apiDefinition->setPublic(true);

        $apiId = 'payum.context.'.$contextName.'.api';
        $container->setDefinition($apiId, $apiDefinition);

        $paymentDefinition->addMethodCall('addApi', array(new Reference($apiId)));
    }

}
