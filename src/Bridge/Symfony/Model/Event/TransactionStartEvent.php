<?php

namespace Payum\Processingkz\Bridge\Symfony\Model\Event;


use Symfony\Component\EventDispatcher\Event;


class TransactionStartEvent extends Event
{

    protected $details;


    public function __construct()
    {
        $this->details = array();
    }

    public function setTransactionDetails($details = array())
    {
        $this->details = $details;
        return $this;
    }

    public function getTransactionDetails()
    {
        return $this->details;
    }

}
