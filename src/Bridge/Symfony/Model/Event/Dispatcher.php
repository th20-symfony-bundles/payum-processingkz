<?php

namespace Payum\Processingkz\Bridge\Symfony\Model\Event;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class Dispatcher
{

    protected $backendDispatcher;


    public function __construct(EventDispatcherInterface $backendDispatcher)
    {
        $this->backendDispatcher = $backendDispatcher;
    }

    public function dispatchTransactionStart(TransactionStartEvent $event = null)
    {
        if (!$event) {
            $event = new TransactionStartEvent();
        }

        return $this->backendDispatcher->dispatch(Events::TRANSACTION_START, $event);
    }

    public function dispatchTransactionStatusSync(TransactionStatusSyncEvent $event = null)
    {
        if (!$event) {
            $event = new TransactionStatusSyncEvent();
        }

        return $this->backendDispatcher->dispatch(Events::TRANSACTION_STATUS_SYNC, $event);
    }

}
