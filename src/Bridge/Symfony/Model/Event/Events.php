<?php

namespace Payum\Processingkz\Bridge\Symfony\Model\Event;


class Events
{

    const TRANSACTION_START = 'payum.processingkz.transaction_start';

    const TRANSACTION_STATUS_SYNC = 'payum.processingkz.transaction_status_sync';

}
