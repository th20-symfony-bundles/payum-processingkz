<?php

namespace Payum\Processingkz\Bridge\Symfony\Model\Event;


use Symfony\Component\EventDispatcher\Event;


class TransactionStatusSyncEvent extends Event
{

    protected $newStatus;

    protected $oldStatus;


    public function setOldStatus($status)
    {
        $this->oldStatus = $status;
        return $this;
    }

    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    public function setNewStatus($status)
    {
        $this->newStatus = $status;
        return $this;
    }

    public function getNewStatus()
    {
        return $this->newStatus;
    }

}
