<?php

namespace Payum\Processingkz;


use DateTime;
use InvalidArgumentException;

use ProcessingKz\Client;
use ProcessingKz\Objects\Entity\TransactionDetails;
use ProcessingKz\Objects\Entity\GoodsItem;
use ProcessingKz\Objects\Request\CompleteTransaction;
use ProcessingKz\Objects\Request\GetTransactionStatus;
use ProcessingKz\Objects\Request\StartTransaction;


/**
 * SoapClient wrapper class for Payum.
 */
class Api
{

    const COMPLETE_SUCCESS = 1;

    const COMPLETE_FAILURE = 2;


    protected $client;

    protected $merchantId;


    public function __construct(array $options)
    {
        $wdsl = null;
        $clientOptions = array();

        if (!empty($options['wsdl'])) {
            $wsdl = $options['wsdl'];
        }
        if (!empty($options['location'])) {
            $clientOptions['location'] = $options['location'];
        }

        $this->client = new Client($wsdl, $clientOptions);
        $this->merchantId = $options['merchant'];
    }

    public function currencyCodeFromName($name)
    {
        switch (strtolower($name)) {
            case 'kzt':
                return 398;

            default:
                $message = sprintf('Unknown currency %s', $name);
                throw new InvalidArgumentException($message);
        }
    }

    public function currencyNameFromCode($code)
    {
        switch ($code) {
            case 398:
                return 'KZT';

            default:
                $message = sprintf('Unknown currency code %s', $code);
                throw new InvalidArgumentException($message);
        }
    }

    public function startTransaction($data = array())
    {
        $details = $this->createTransactionDetails($data);
        $transaction = new StartTransaction();
        $transaction->setTransaction($details);

        $result = $this->client->startTransaction($transaction);
        return $result->getReturn();
    }

    public function checkStatus($data = array())
    {
        $reference = $data['customerReference'];

        $status = new GetTransactionStatus();
        $status->setMerchantId($this->merchantId);
        $status->setReferenceNr($reference);

        $result = $this->client->getTransactionStatus($status);
        return $result->getReturn();
    }

    public function completeTransaction($data = array(), $result = self::COMPLETE_SUCCESS)
    {
        if (empty($data['customerReference'])) {
            return;
        }
        if (empty($data['transactionStatus']) || $data['transactionStatus'] !== 'AUTHORISED') {
            // Only authorised transactions can be completed.
            return;
        }

        $reference = $data['customerReference'];

        $complete = new CompleteTransaction();
        $complete->setMerchantId($this->merchantId);
        $complete->setReferenceNr($reference);

        switch ($result) {
            case self::COMPLETE_SUCCESS:
                $complete->setTransactionSuccess(true);
                break;

            case self::COMPLETE_FAILURE:
            default:
                $complete->setTransactionSuccess(false);
        }

        $result = $this->client->completeTransaction($complete);
        return $result->getReturn();
    }

    protected function createTransactionDetails($data)
    {
        $date = new DateTime();

        $details = new TransactionDetails();
        $details->setMerchantId($this->merchantId);
        $details->setMerchantLocalDateTime($date->format('d.m.Y H:i:s'));

        if (!empty($data['language'])) {
            $details->setLanguageCode($data['language']);
        }
        if (!empty($data['currency_code'])) {
            $details->setCurrencyCode($data['currency_code']);
        }
        if (!empty($data['order_id'])) {
            $details->setOrderId($data['order_id']);
        }
        if (!empty($data['email'])) {
            $details->setPurchaserEmail($data['email']);
        }
        if (!empty($data['return_url'])) {
            $details->setReturnURL($data['return_url']);
        }
        if (!empty($data['total'])) {
            $details->setTotalAmount($data['total']);
        }

        if (!empty($data['items'])) {
            foreach ($data['items'] as $k => $v) {
                $item = new GoodsItem();
                $item->setNameOfGoods(trim($k));
                $item->setAmount($v);

                if (!empty($data['currency_code'])) {
                    $item->setCurrencyCode($data['currency_code']);
                }

                $details->addGoodsItem($item);
            }
        }

        return $details;
    }

}
