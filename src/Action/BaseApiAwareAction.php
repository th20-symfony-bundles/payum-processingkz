<?php

namespace Payum\Processingkz\Action;


use Payum\Core\ApiAwareInterface;
use Payum\Core\Action\PaymentAwareAction;

use Payum\Processingkz\Api;


abstract class BaseApiAwareAction extends PaymentAwareAction implements ApiAwareInterface
{

    /**
     * {@inheritdoc}
     */
    public function setApi($api)
    {
        if (false == $api instanceof Api) {
            throw new UnsupportedApiException('Not supported.');
        }
        $this->api = $api;
    }

}
